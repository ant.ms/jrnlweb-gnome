import gi 	
import sqlite3

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gio

dates = []

postTitle = Gtk.Label()
postContent = Gtk.Label()

class HeaderBarWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="JrnlWeb - Gnome")
        # self.set_border_width(10)
        self.set_default_size(620, 400)

        hb = Gtk.HeaderBar()
        hb.set_show_close_button(True)
        hb.props.title = "JrnlWeb - Gnome"
        # hb.set_has_subtitle(True)
        # hb.props.subtitle = "JrnlWeb - Gnome"
        self.set_titlebar(hb)

        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        Gtk.StyleContext.add_class(box.get_style_context(), "linked")

        button = Gtk.Button()
        button.add(Gtk.Arrow(Gtk.ArrowType.LEFT, Gtk.ShadowType.NONE))
        box.add(button)

        button = Gtk.Button()
        button.add(Gtk.Arrow(Gtk.ArrowType.RIGHT, Gtk.ShadowType.NONE))
        box.add(button)

        hb.pack_start(box)

        dateComboBox = Gtk.ComboBoxText()
        dateComboBox.set_entry_text_column(0)
        dateComboBox.connect("changed", self.on_date_combobox_changed)
        for element in dates:
            dateComboBox.append_text(element)

        hb.pack_start(dateComboBox)

        postTitle.set_line_wrap(True)
        postContent.set_line_wrap(True)


        listbox = Gtk.ListBox()
        listbox.set_selection_mode(Gtk.SelectionMode.NONE)
        listbox.add(postTitle)
        listbox.add(postContent)

        self.add(listbox)
    
    def on_date_combobox_changed(self, combo):
        dateOfPosts = combo.get_active_text()
        selectiveCursor = db.cursor()
        selectiveCursor.execute("SELECT * FROM posts WHERE dateofcreation BETWEEN '" + dateOfPosts + " 00:00:00' AND '" + dateOfPosts + " 23:59:59'")
        for row in selectiveCursor:
            postTitle.set_text("<b>" + row[1] + "</b>")
            postTitle.set_use_markup(True)
            postContent.set_text(row[2])

db = sqlite3.connect('posts.sqlite')

cursor = db.cursor()
cursor.execute("SELECT strftime('%Y-%m-%d', dateofcreation) AS dates FROM posts GROUP BY dates ORDER BY dates DESC")
for row in cursor:
    dates.append(row[0])

win = HeaderBarWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()

db.close()